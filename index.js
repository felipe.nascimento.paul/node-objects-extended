/* String extension methods */
/**
 * Replace all strings into a string
 * @param {string} oldvalue - The string to be replaced
 * @param {string} newvalue - TThe string to replace all occurrences of oldvalue
 */
String.prototype.replaceAll = function (oldvalue, newvalue) {
  return this.split(oldvalue).join(newvalue);
};
String.prototype.getHash = function () {
  var hash = 0,
    i,
    chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr = this.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0;
  }
  return hash;
};
String.prototype.toTitleCase = function () {
  return this.replace(
    /\w\S*/g,
    txt =>
    txt.length > 1 ?
    txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() :
    txt
  );
};
String.prototype.truncate = function(length, separator) {
  if (this.length <= length) return this

  separator = separator || '...'

  var sepLen = separator.length,
    charsToShow = length - sepLen,
    frontChars = Math.ceil(charsToShow / 2),
    backChars = Math.floor(charsToShow / 2)

  return (
    this.substr(0, frontChars) +
    separator +
    this.substr(this.length - backChars)
  )
}
/* Number extension methods */
Number.prototype.toTimeString = function (format) {
  format = format || "";
  let current = this;
  let days = Math.floor(current / 86400);
  current = current % 86400;
  let hours = Math.floor(current / 3600);
  current = current % 3600;
  let minutes = Math.floor(current / 60);
  current = current % 60;
  let seconds = Math.floor(current);
  days = days === 0 ? "" : days < 10 ? `0${days}D ` : `${days}D `;
  hours = hours < 10 ? `0${hours}` : `${hours}`;
  minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
  seconds = seconds < 10 ? `0${seconds}` : `${seconds}`;
  return `${days + hours}:${minutes}:${seconds}`;
};
Number.prototype.withSeparator = function(separator, fixed) {
  var x = fixed >= 0 ? this.toFixed(fixed) : this.toString()
  if (this === eval(x)) x = this.toString()
  var pattern = /(-?\d+)(\d{3})/
  while (pattern.test(x)) x = x.replace(pattern, '$1' + separator + '$2')
  return x
}
Number.prototype.formatUnit = function(unit, separator, decimals) {
  separator = separator || ''
  decimals = decimals === null || decimals === undefined ? 1 : decimals
  switch (unit) {
    case 'bytes':
      if (this > 1099511627776) {
        return `${(this / 1099511627776).withSeparator(',', decimals)}${
          separator
        }TB`
      }
      if (this > 1073741824) {
        return `${(this / 1073741824).withSeparator(',', decimals)}${
          separator
        }GB`
      }
      if (this > 1048576) {
        return `${(this / 1048576).withSeparator(',', decimals)}${separator}MB`
      }
      if (this > 1024) {
        return `${(this / 1024).withSeparator(',', decimals)}${separator}KB`
      }
      return this
      break
    case 'count':
      if (this > 1000000000000) {
        return `${(this / 1000000000000).withSeparator(',', decimals)}${
          separator
        }T`
      }
      if (this > 1000000000) {
        return `${(this / 1000000000).withSeparator(',', decimals)}${
          separator
        }B`
      }
      if (this > 1000000) {
        return `${(this / 1000000).withSeparator(',', decimals)}${separator}M`
      }
      if (this > 1000) {
        return `${(this / 1000).withSeparator(',', decimals)}${separator}K`
      }
      return this
      break
    case 'ms':
      if (this > 86400000) {
        return `${(this / 86400000).withSeparator(',', decimals)}${separator}d`
      }
      if (this > 3600000) {
        return `${(this / 3600000).withSeparator(',', decimals)}${separator}hr`
      }
      if (this > 60000) {
        return `${(this / 60000).withSeparator(',', decimals)}${separator}min`
      }
      if (this > 1000) {
        return `${(this / 1000).withSeparator(',', decimals)}${separator}sec`
      }
      return `${this}${separator}ms`
      break
    case '%':
      return `${this}${separator}%`
      break
    default:
      return this
      break
  }
}
/* Array extension methods */
Array.prototype.max = function(func) {
  return Math.max.apply(Math, this.map(func))
}
/* Date extension methods */
Date.prototype.toString = function (format) {
  format = format || "yyyy-MM-ddTHH:mm:ssZ";
  var MM = this.getMonth() + 1;
  var dd = this.getDate();
  var yyyy = this.getFullYear();
  var HH = this.getHours();
  var mm = this.getMinutes();
  var ss = this.getSeconds();
  var T = -(this.getTimezoneOffset() / 60);
  var s = T < 0 ? "-" : "+";
  MM = MM < 10 ? `0${MM}` : MM;
  dd = dd < 10 ? `0${dd}` : dd;
  HH = HH < 10 ? `0${HH}` : HH;
  mm = mm < 10 ? `0${mm}` : mm;
  ss = ss === 0 ? "00" : ss < 10 ? `0${ss}` : ss;
  T = Math.abs(T);
  var Z = s + (T < 10 ? `0${T}:00` : `${T}:00`);
  var str = format
    .replaceAll("yyyy", yyyy)
    .replaceAll("MM", MM)
    .replaceAll("dd", dd)
    .replaceAll("HH", HH)
    .replaceAll("mm", mm)
    .replaceAll("ss", ss)
    .replaceAll("Z", Z);
  return str;
};
Date.prototype.toUTCString = function (format) {
  format = format || "yyyy-MM-ddTHH:mm:ssZ";
  var MM = this.getUTCMonth() + 1;
  var dd = this.getUTCDate();
  var yyyy = this.getUTCFullYear();
  var HH = this.getUTCHours();
  var mm = this.getUTCMinutes();
  var ss = this.getUTCSeconds();
  MM = MM < 10 ? `0${MM}` : MM;
  dd = dd < 10 ? `0${dd}` : dd;
  HH = HH < 10 ? `0${HH}` : HH;
  mm = mm < 10 ? `0${mm}` : mm;
  ss = ss === 0 ? "00" : ss < 10 ? `0${ss}` : ss;
  var Z = "+00:00";
  var str = format
    .replaceAll("yyyy", yyyy)
    .replaceAll("MM", MM)
    .replaceAll("dd", dd)
    .replaceAll("HH", HH)
    .replaceAll("mm", mm)
    .replaceAll("ss", ss)
    .replaceAll("Z", Z);
  return str;
};
Date.toDate = function(str) {
  const aux = str.split('T')
  const date = aux[0].split('-')
  const y = date[0]
  let M = date[1]
  const d = date[2]
  const time = aux.length > 1 ? aux[1].split(':') : []
  const H = time.length < 1 ? 0 : time[0]
  const m = time.length < 2 ? 0 : time[1]
  const s = time.length < 3 ? 0 : time[2]
  M = parseInt(M) - 1
  return new Date(y, M, d, H, m, s)
}
Date.today = function() {
  const date = new Date()
  date.setHours(0)
  date.setMinutes(0)
  date.setSeconds(0)
  date.setMilliseconds(0)
  return date
}
Date.prototype.addYear = function (y) {
  this.setFullYear(this.getFullYear() + y);
  return this;
};
Date.prototype.addMonths = function (m) {
  this.setMonth(this.getMonth() + m);
  return this;
};
Date.prototype.addDays = function (d) {
  this.setDate(this.getDate() + d);
  return this;
};
Date.prototype.addHours = function (h) {
  this.setHours(this.getHours() + h);
  return this;
};
Date.prototype.addMinutes = function (m) {
  this.setMinutes(this.getMinutes() + m);
  return this;
};
Date.prototype.addSeconds = function (s) {
  this.setSeconds(this.getSeconds() + s);
  return this;
};
Date.prototype.addMilliseconds = function (s) {
  this.setMilliseconds(this.getMilliseconds() + s);
  return this;
};
Date.prototype.resetTime = function () {
  this.setHours(0);
  this.setMinutes(0);
  this.setSeconds(0);
  this.setMilliseconds(0);
  return this;
};
module.exports = {};
